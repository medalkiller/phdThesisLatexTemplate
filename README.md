# Ph.D Manuscript Latex Template 

This repository contains a multilingue Latex template and example (Fr and En) for Ph.D Thesis Manuscript.
French and English language can be selected.
Other language can be added as an extra latex file (see: src/language_en.tex for example)

# Features 

- Multilanguage (Fr ans En are provided, more can be added)
- Glossary 
- Bibliography with biber 
- UTF-8 
- Adapted to French and english documents
- Makefile 
- Chapter with quotation
- Personnal publications list

# Repository structure

- **src/** : source files 
    - phdThesisTemplate.cls : Common language Latex template 
    - language_****.tex : language specific template 
    
- **example/** : contains example src files for both Fr and En PhD thesis with one Makefile for compilation

# Example 

1. Modify Makefile variable *MANUSCRIPT_FILE* to **Manuscript_en** or **Manuscript_fr** to either compile Fr or En example 

2. Make 

3. Open *Manuscript_xx.pdf*

A complete thesis manuscript in French is given in file *example_thesis_Baumeyer.pdf*



# Dependencies 

- Latex distribution
- gnuplot
- biber

## Needed Latex Packages 

- hypperref
- inputenc
- lmodern
- fontenc
- grrfile
- ifpdf
- ifthen
- chnpage
- standalone
- multirow
- gnuplot-lua-tikz
- tikz
- icomma
- amsmath
- amsfonts
- amssymb
- graphicx
- subfig
- epstopdf
- caption
- blindtext
- geometry
- fancyhdr
- fncychap
- hyperref
- xcolor
- tabularx
- tabulary
- booktabs
- multirow
- glossaries
- epigraph
- url
- pdfpages
- color
- textcomp
- floatrow
- blindtext
- enumerate
- empheq
- listings
- adjustbox
- csquotes
- babel
- minitoc
- biblatex

