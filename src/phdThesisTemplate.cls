\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{phdThesisTemplate}[1]
\LoadClass[11pt,twoside,openright,a4paper]{report} 


%------------------------------------------------
% Packages pour la gestion de la langue
%------------------------------------------------
\usepackage[utf8]{inputenc} % Permet de saisir directement les caractères accentués (par ex. é au lieu de \'e
\usepackage{lmodern} %beau lissage des caractères
\usepackage[T1]{fontenc}    % Sélection des fontes recommandées
\usepackage[encoding,filenameencoding=utf8]{grffile} % Pour les nom de fichier et dossier avec accents (UTF8 partout)


%------------------------------------------------
% Profondeur numérotation et références
%------------------------------------------------
\setcounter{secnumdepth}{3}
\sloppy
\usepackage{ifpdf}          % Permet de détecter si l'on compile avec pdflatex ou pas
\usepackage{ifthen}         % Permet l'utilisation des booléens
\usepackage{chngpage}       % définit des fonctions pour déterminer si la page est paire ou non


%------------------------------------------------
%Package for scheme For latex tikz schema (types algorithmes)
%------------------------------------------------
\usepackage{standalone} 
\usepackage{multirow}
\usepackage{gnuplot-lua-tikz}
\usepackage{tikz}
\usetikzlibrary{shadows,arrows, shapes}


%------------------------------------------------
% Packages pour les maths
%------------------------------------------------
\usepackage{icomma} %Le séparateur décimal est une virgule
\usepackage{amsmath}    % The package pour les maths : définit plusieurs environnements pour les alignements, les matrices, etc...
\usepackage{amsfonts}   % Ajoute des fontes utilisables en mode mathématique
\usepackage{amssymb}    % Définit des symboles mathémtiques supplémentaires
\DeclareMathOperator{\sign}{sign}  
\relpenalty=10000       % empêche la césure des équations
\binoppenalty=10000


%------------------------------------------------
% Packages pour les figures 
%------------------------------------------------
\usepackage{graphicx}       % Affichage des figures
\usepackage{subfig}			% Subfigures 
\usepackage{epstopdf}		% Pour convertir les figures .eps en .pdf 
\usepackage[font=small,labelfont=bf]{caption}


%------------------------------------------------
% Packages pour l'esthétisme et la mise en page
%------------------------------------------------
\usepackage{blindtext}
\usepackage[margin=2.5cm,includeheadfoot]{geometry}   % Définit les marges du document
\usepackage{fancyhdr}         % Pour les en-têtes et les pieds de page avec un liseret
\usepackage[Sonny]{fncychap}  % Permet de déinir le style de vos en-têtes de chapitre
\ifpdf
    \usepackage[pdftex]{hyperref}                                                            
\else
    \usepackage[hypertex]{hyperref}
\fi
\usepackage{xcolor} %Pour gérer les couleurs
\usepackage{tabularx} %Pour le saut de ligne automatique dans les tableaux
\usepackage{tabulary} %Package alternatif à tabularx
\usepackage{booktabs}
\usepackage{multirow} %Pour la fusion des cellules dans les tableaux
\newcolumntype{C}{>{\centering}X} %pour centrer en déclaration, pour remplacer les \\ du tableau par des \tabularnewline


%------------------------------------------------
% Packages pour le glossaire
%------------------------------------------------
\usepackage[acronym,xindy,toc,numberedsection]{glossaries}
\makeglossaries
\usepackage{epigraph}


%------------------------------------------------
% Gestion interligne etc...
%------------------------------------------------
\setlength{\parskip}{2ex plus 0.5ex minus 0.5ex}    % saut de ligne entre paragraphe


%------------------------------------------------
% Autres packages utiles
%------------------------------------------------
\usepackage{url}            % Gestion des url dans la bibliographie et autre
\usepackage{pdfpages}       % Permet l'inclusion de pages déjà en pdf
\usepackage{color}
\usepackage{textcomp}
\usepackage{floatrow}
\newfloatcommand{capbtabbox}{table}[][\FBwidth] % Table float box with bottom caption, box width adjusted to content
\usepackage{blindtext}
\usepackage{enumerate}
\usepackage[overload]{empheq}
\usepackage{listings}			% Inclusion de code source informatique
\usepackage{adjustbox}
\usepackage{csquotes}


%------------------------------------------------
%    Définition de commances personnelles
%------------------------------------------------
%%% Fonction abs 
\newcommand{\abs}[1]{\lvert#1\rvert} %Commande abs

%%% Traits horizontaux 
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}

%%%1re  page de tout chapitre
\fancypagestyle{plain}{
    \renewcommand{\headrulewidth}{0pt}
    \fancyhead[LO]{}
    \fancyhead[LE]{}
  \cfoot{\thepage}
}
\pagestyle{empty}
\fancyhead{}    % en-tête vide
\fancyfoot{}    % bas de page vide
\setlength{\headheight}{15pt} 


%------------------------------------------------
% Quelques petites modifs apportées au style d'origine
%-----------------------------------------------------
\renewcommand{\labelitemi}{$\bullet$} %pour changer le style du niveau i des items


%------------------------------------------------
% Gestion des pages impaires/paires
%------------------------------------------------
% Ajoute si nécessaire une page blanche pour que la page suivante soit impaire (à droite)
\newcommand{\clearemptydoublepage}{%
    \ifthenelse{\isodd{\value{page}}}{\newpage{\thispagestyle{empty}}}{}\cleardoublepage\pagestyle{fancy}
}
    

%------------------------------------------------
% Gestion de l'introduction, de la conclusion
%------------------------------------------------	
%%% Chapitre sans numéro (intro, conclusion, etc.)
\newcommand{\chapitresansnum}[1]{%
	\clearemptydoublepage%
	\chapter*{#1}%
	\addstarredchapter{#1}
	\cfoot{\thepage}
	\fancyhead[LE]{\textsc {#1}}%
	\fancyhead[LO]{\hfill \textsc {#1}}%
}


%------------------------------------------------
% Gestion des chapitres sans mini-table
%------------------------------------------------	
%%%  Chapitre sans table des matières 
\newcommand{\chapitresanstdm}[1]{%
	\clearemptydoublepage%
	\chapter{#1}%
	\cfoot{\thepage}
	\fancyhead[LO]{\hfill\textsc {\nouppercase \rightmark}}%
	\fancyhead[LE]{\textsc {\nouppercase \leftmark}}%
}
